libmonitoring-livestatus-perl (0.84-3) UNRELEASED; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.7.0, no changes.
  * Bump debhelper compat to 13.
  * Enable Salsa CI.

 -- Bas Couwenberg <sebastic@debian.org>  Wed, 18 Jan 2023 17:28:26 +0100

libmonitoring-livestatus-perl (0.84-2) unstable; urgency=medium

  * Team upload.
  * Bump Standards-Version to 4.6.1, no changes.
  * Add Rules-Requires-Root to control file.
  * Update Vcs-* URLs for repo rename.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 02 Dec 2022 09:45:06 +0100

libmonitoring-livestatus-perl (0.84-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Bump Standards-Version to 4.5.1, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 18 Dec 2020 05:35:16 +0100

libmonitoring-livestatus-perl (0.80-2) unstable; urgency=medium

  * Team upload.

  [ Bas Couwenberg ]
  * Bump Standards-Version to 4.3.0, no changes.
  * Bump debhelper compat to 10, changes:
    - Drop --parallel option, enabled by default
  * Change netcat build dependency to netcat-openbsd.
    (closes: #969236)
  * Bump watch file version to 4.
  * Add patch to fix interpreter path in examples.
  * Update gbp.conf to use --source-only-changes by default.

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).
  * Update standards version to 4.5.0, no changes needed.

 -- Bas Couwenberg <sebastic@debian.org>  Fri, 20 Nov 2020 10:26:40 +0100

libmonitoring-livestatus-perl (0.80-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Change libjson-xs-perl (build) dependency to libcpanel-json-xs-perl.
  * Drop patches, applied upstream.
  * Add 'Testsuite: autopkgtest-pkg-perl' to control file.
  * Update Vcs-* URLs for Salsa.
  * Add gbp.conf to use pristine-tar by default.
  * Add upstream metadata.
  * Bump Standards-Version to 4.1.4, no changes.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 05 May 2018 19:55:57 +0200

libmonitoring-livestatus-perl (0.78-1) unstable; urgency=medium

  * Team upload.
    (closes: #849207)
  * New upstream release.
    (closes: #838976)
  * Restructure control file with cme.
  * Update Vcs-Git URL to use HTTPS.
  * Use metacpan.org instead of search.cpan.org.
  * Update copyright file using copyright-format 1.0.
  * Bump debhelper compatibility to 9.
  * Enable parallel builds.
  * Switch to source format 3.0 (quilt).
  * Add build dependencies on procps & netcat for tests.
  * Bump Standards-Version to 3.9.8, changes: copyright-format 1.0.
  * Add patch to increase sleep to allow netcat to start.

 -- Bas Couwenberg <sebastic@debian.org>  Sat, 14 Jan 2017 13:32:43 +0100

libmonitoring-livestatus-perl (0.74-1) unstable; urgency=low

  * New upstream release
  * Bump standards version (no changes)

 -- Alexander Wirt <formorer@debian.org>  Sun, 05 Jun 2011 21:51:34 +0200

libmonitoring-livestatus-perl (0.64-1) unstable; urgency=low

  * Initial Release.

 -- Alexander Wirt <formorer@debian.org>  Tue, 1 Feb 2011 16:13:34 +0100
